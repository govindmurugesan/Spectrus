$(window).scroll(function() {
	scrollTop = $(window).scrollTop(),
	divOffset = $('.home_banner,.about_banner,.contact_banner').offset().top,
	dist = (divOffset - scrollTop);
	if (dist <= -10) {
		$('#scroll').addClass("scroll_animation");
		$('.menu_items').addClass('scroll_menu');
		$('.menu_items').addClass('scroll_menu');
		$('.menu_items').removeClass('ladding_menu');
		$('.menu_lines').addClass('menu_lines_1');
		$('.logo_img1').addClass('ladding_logo');
		$('.logo_img2').addClass('scroll_logo');
	} else {
		$('#scroll').removeClass("scroll_animation");
		$('.menu_items').removeClass('scroll_menu');
		$('.menu_items').removeClass('scroll_menu');
		$('.menu_items').addClass('ladding_menu');
		$('.menu_lines').removeClass('menu_lines_1');
		$('.logo_img2').removeClass('scroll_logo');
		$('.logo_img1').removeClass('ladding_logo');
	}
});
$(document).ready(function(){
	$('.menu_lines').click(function(){
		$('html').css("overflow","hidden");
		$('#ovrlay').addClass('menu_overlay');
		$('.main_menu').addClass('open_menu');
		$("#wrapper_container").addClass('wrapr');
	});
	$('.close').click(function(){		
		$('html').css("overflow","auto");
		$('#ovrlay').removeClass('menu_overlay');
		$('.main_menu').removeClass('open_menu');
		$("#wrapper_container").removeClass('wrapr');
		$('body').css("overflow-y","auto");
	});
});
jQuery(document).ready(function() {
    jQuery('.post').addClass("ani_hidden").viewportChecker({
        classToAdd: 'ani_visible animated fadeInDown',
        offset: 100
       });
});
